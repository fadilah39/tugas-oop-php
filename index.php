<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "name : " . $sheep->name. "<br>";
    echo "legs : " .$sheep->legs. "<br>";
    echo "cold blooded : " .$sheep->cold_blooded. "<br> <br>";

    $kodok = new Frog("buduk");
    echo "name : " . $kodok->name. "<br>";
    echo "legs : " . $kodok->legs. "<br>";
    echo "cold blooded : " . $kodok->cold_blooded. "<br>";
    echo $kodok->jump() . "<br> <br>" ;

    $sungokong = new Ape("kera sakti");
    echo "name : " . $sungokong->name. "<br>";
    echo "legs : " . $sungokong->legs. "<br>";
    echo "cold blooded : " . $sungokong->cold_blooded. "<br>";
    echo $sungokong->yell();



?>